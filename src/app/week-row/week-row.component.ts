import { Component, OnInit,Input } from '@angular/core';
import {week} from '../../intefaces/week';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {
  @Input() weekDay:week;
  @Input() i:number;
  constructor() { 
    
  }
  ngOnInit(): void {
  
  }
}

import { Component, OnInit } from '@angular/core';
import { RouterLinkWithHref } from '@angular/router';
import { status } from '../../intefaces/status';
import { week } from '../../intefaces/week';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  //Variable
  status: status[] = [];
  week: week;
  //Logo_Images
  myimage: string = 'assets/images/pttlogo.jpg';
  constructor() {}
  //Function for random data
  randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  ngOnInit(): void {
    //Declaration status interface
    this.status = [
      { used: this.randomInteger(30, 90), total: 90 },
      { used: this.randomInteger(30, 80), total: 80 },
      { used: this.randomInteger(30, 100), total: 100 },
    ];
  }
  //Method return weekRow
  generateWeekRow(): any {
    return {
      name: 'week',
      reqular: { used: this.randomInteger(30, 90), total: 90 },
      diesel: { used: this.randomInteger(30, 80), total: 80 },
      premium: { used: this.randomInteger(30, 100), total: 100 },
    };
  }
}
